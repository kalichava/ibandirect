window.app = window.app || {}

app.cacheSelectors = () => {
    app.cache = {
      // General
      $html                    : $('html'),
      $body                    : $(document.body),

      // Header
      $navMainItemLink         : $('.nav__main__item__link'),
      $navMainItemLinkSubmenu  : $('.nav__main__item__submenu'),
      $hamburger               : $('.hamburger'),

      // Faq
      $faqQuestion             : $('.faqList__group__item__question')
    }
}

app.menu = () => {
    const showSubmenu = e => {
        const nextEl = $(e.target).next()
        if(nextEl.hasClass('nav__main__item__submenu')) {
            nextEl.addClass('active')
        }
    }

    const hideSubmenu = e => {
        if(e.currentTarget.localName == 'body') {
            app.cache.$navMainItemLinkSubmenu.removeClass('active')
        } else if($(e.currentTarget).hasClass('nav__main__item__submenu')) {
            $(e.currentTarget).removeClass('active')
        } 
    }

    app.cache.$hamburger.click(e => {
        $(e.currentTarget).toggleClass('is-active')
        app.cache.$body.toggleClass('menu-active')
    })
    app.cache.$navMainItemLink.hover(showSubmenu)
    app.cache.$navMainItemLinkSubmenu.hover(null, hideSubmenu)
    app.cache.$body.click(hideSubmenu)
}

app.faq = () => {
    app.cache.$faqQuestion.click(e => { $(e.currentTarget).parent().toggleClass('active')})
}

app.init = () => {
    app.cacheSelectors()
    app.menu()
    app.faq()
}

$(app.init)